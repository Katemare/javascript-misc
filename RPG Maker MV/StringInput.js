//=============================================================================
// StringInput.js
//=============================================================================

/*:
 * @plugindesc Input string variables via reworked Naming screen
 * @author EvilCat
 * @email soevilcat@mail.ru
 * @version 1.2
 
 * @help
 * Allows input of strings (words) up to 16 characters and storing the result in
 * a variable.
 *
 * Plugins Commands:
 *
 *   StringInput 8=>100
 * inputs up to 8 chars and stores the result in var 100.
 *
 *   StringInput 4-8=>100
 * same as above, but at least 4 non-space characters is required.
 *
 * Use Plugin Command "Calc" from EvilCat Utils to analyze input.
 *
 *   StringInput Default meow
 * initially the inputted string will me "meow".
 * note that if you set default string to something that will be considered
 * invalid by following checks, the player won't be able to send it!
 *
 *   StringInput SetFace Harold
 * sets face displayed in input window to actor's by name of Harold.
 *
 *   StringInput SetFace "Harold Smith"
 * same as above, but for names containing a space.
 *
 *   StringInput SetFace #3
 * sets face displayed in input window to Actor ID 3 (regargless of name).
 *
 *   StringInput SetFace Nature 2
 * sets face displayed in input window to 3rd (counting from zero) in faceset "Nature" (a cat).
 *
 *   StringInput NoFace
 * no face is displayed (default)
 *
 *   StringInput Verify Word
 * only "word" input is allowed (no spaces). default.
 * also available are "Latin" (only A-Z) and "Russian" (А-Я).
 * player can still input invalid characters, but won't be able to send the input.
 *
 *   StringInput RegExp /meow/i
 * only allows input that includes 'meow' regargless of capitalization.
 * works in addition to Verify instuction above.
 * See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
 *
 *   StringInput NoVerify
 * disables both verification and regexp.
 *
 *   StringInput Process Trim
 * input is trimmed for spaces on each change. default.
 * also available are "LC" (lowercase), "UC" (UPPERCASE) and "UCfirst" (Capitalization).
 * 
 *   StringInput Process Trim UC
 * both trims spaces and converts to uppercase.
 *
 *   StringInput NoProcess
 * disables processing.
 *
 *   StringInput Lang Russian
 * sets input language to Russian; Russian alphabet will be used.
 * unless you do it, people with non-Russian OS won't be able to input Russian letters!
 * also available are "Latin" and "Japanese"
 * by default, OS language is used.
 *
 *   StringInput Reset
 * resets to default values.
 * otherwise, settings remain until the end of event's execution.
 *
 * Creative Commons 4.0 Attribution license
 */

"use strict";

if (!EvilCat) throw new Error('Requires EvilCat Utils plugin!');

(function()
{
	var StringInput = EvilCat.StringInput = function StringInput()
	{
		EvilCat.Plugin.call(this);
	}
	EvilCat.extend(StringInput, EvilCat.Plugin);
	
	StringInput.prototype.makeCommandsList=function()
	{ this.validCommands=['SetFace', 'NoFace', 'Verify', 'NoVerify', 'RegExp', 'Process', 'NoProcess', 'Reset', 'Default', 'Lang']; }

	StringInput.prototype._eventCommand=function(plugin_name, interpreter, args)
	{
		var original=args.join(' ');
		var input=/^\s*(\d+)\s*(\-\s*(\d+))?\s*=>\s*(.+?)\s*$/.exec(original);
		if (input)
		{
			var n=Number(input[1]), m=Number(input[3]), v=Number(input[4]);
			if (!m)
			{
				m=n;
				n=this.DEFAULT_MIN_INPUT_LENGTH;
			}
			else if (m<n) throw new Error('max less then min for required characters!');
			if (m>this.MAX_INPUT_LENGTH) throw new Error('can\'t input more than 16 characters!');
			// anything goes for variable index for compatibility with other possible plugins.
			this.processInput(n, m, v, this._getSettingsFor(interpreter) );
		}
		else EvilCat.Plugin.prototype._eventCommand.apply(this, arguments);
	}
	
	StringInput.prototype._getSettingsFor=function(interpreter)
	{
		return interpreter._StringInput_settings || (interpreter._StringInput_settings=new StringInputSettings());
	}
	StringInput.prototype._clearSettingsFor=function(interpreter)
	{
		delete interpreter._StringInput_settings;
	}
	
	StringInput.prototype.Default=function(plugin_name, interpreter)
	{
		var rest=Array.prototype.slice.call(arguments, 2).join(' ');
		this._getSettingsFor(interpreter).setDefault(rest);
	}
	StringInput.prototype.SetFace=function(plugin_name, interpreter, source, index)
	{
		var original=Array.prototype.slice.call(arguments, 2).join(' ');
		var match=/^\s*"(.+)"\s*$/.exec(original);
		var actorName, actor;
		if (match) actorName=match[1];
		else if (index===undefined)
		{
			if (source[0]==='#') actor=$gameActors.actor(source.substr(1));
			else actorName=source;
		}
		
		if (actorName)
		{
			for (var x=1; x<$dataActors.length; x++)
			{
				var test=$gameActors.actor(x);
				if (test.name()===actorName)
				{
					actor=test;
					break;
				}
			}
			if (!actor) throw new Error('Actor '+source+' not found!');
		}
		if (actor) this._getSettingsFor(interpreter).setActorFace(actor.actorId());
		else if (index===undefined) throw new Error('Unknow actor');
		else this._getSettingsFor(interpreter).setFace(source, index);
	}
	StringInput.prototype.NoFace=function(plugin_name, interpreter)
	{
		this._getSettingsFor(interpreter).clearFace();
	}
	StringInput.prototype.Verify=function(plugin_name, interpreter, type)
	{
		this._getSettingsFor(interpreter).setVerify(type);
	}
	StringInput.prototype.NoVerify=function(plugin_name, interpreter)
	{
		this._getSettingsFor(interpreter).clearVerify();
		this._getSettingsFor(interpreter).clearRegExp();
	}
	StringInput.prototype.Process=function(plugin_name, interpreter)
	{
		this._getSettingsFor(interpreter).setProcess(Array.prototype.slice.call(arguments, 2));
	}
	StringInput.prototype.NoProcess=function(plugin_name, interpreter)
	{
		this._getSettingsFor(interpreter).clearProcess();
	}
	StringInput.prototype.RegExp=function(plugin_name, interpreter, ex)
	{
		var reg, match;
		ex=ex.trim();
		if (match=/^\/(.+)\/([^\/]*)$/.exec(ex)) reg=new RegExp(match[1], match[2]);
		else reg=new RegExp(ex);
		this._getSettingsFor(interpreter).setRegExp(reg);
	}
	StringInput.prototype.Lang=function(plugin_name, interpreter, lang)
	{
		this._getSettingsFor(interpreter).setLanguage(lang);
	}
	StringInput.prototype.Reset=function(plugin_name, interpreter)
	{
		this._clearSettingsFor(interpreter);
	}
	
	StringInput.prototype.processInput=function(min, max, varname, settings)
	{
		SceneManager.push(Scene_StringInput)
		SceneManager.prepareNextScene(min, max, varname, settings);
	}
	
	StringInput = EvilCat.StringInput = new StringInput();
	
	StringInput.MAX_INPUT_LENGTH		=16;
	StringInput.DEFAULT_MIN_INPUT_LENGTH=1;
	
	StringInput.verify=
	{
		word: function(input)
		{
			return EvilCat.toBool(/^[^\s]+$/.exec(input));
		},
		latin: function(input)
		{
			return !(/[^a-z]$/i.exec(input));
		},
		russian: function(input)
		{
			return !(/[^а-яё]$/i.exec(input));
		}
	};
	
	StringInput.process=
	{
		trim: function(input)
		{
			return input.trim();
		},
		lc: function(input)
		{
			return input.toLowerCase();
		},
		uc: function(input)
		{
			return input.toUpperCase();
		},
		ucfirst: function(input)
		{
			return input.charAt(0).toUpperCase()+input.substr(1).toLowerCase();
		},
	};
	
	var StringInputSettings=function()
	{
		this.face	=null;
		this.verify	= StringInput.verify.word;
		this.regexp	=null;
		this.process=[];
		this.default='';
		this.lang=null;
	}
	
	StringInputSettings.prototype.setDefault=function(str) 			{ this.default=str; 			}	
	StringInputSettings.prototype.setActorFace=function(actorId)	{ this.face=actorId;			}
	StringInputSettings.prototype.setFace=function(faceset, index)	{ this.face=[faceset, index];	}
	StringInputSettings.prototype.clearFace=function() 				{ this.face=null;				}
	StringInputSettings.prototype.actorId=function()				{ return (EvilCat.isNumber(this.face) && this.face) || null; }
	StringInputSettings.prototype.actor=function()
	{
		var actorId=this.actorId();
		if (actorId) return $gameActors.actor(actorId);
	}
		
	StringInputSettings.prototype.setVerify=function(type)
	{
		type=type.trim().toLowerCase();
		if (!StringInput.verify.hasOwnProperty(type)) throw new Error('bad verify type: '+type);
		this.verify=StringInput.verify[type];
	}
	StringInputSettings.prototype.clearVerify=function()	{ this.verify=null;		}
	
	StringInputSettings.prototype.setProcess=function(types)
	{
		this.process.length=0;
		for (var x=0; x<types.length; x++)
		{
			var type=types[x];
			type=type.trim().toLowerCase();
			if (!StringInput.process.hasOwnProperty(type)) throw new Error('bad process type: '+type);
			this.process.push(StringInput.process[type]);
		}
	}
	StringInputSettings.prototype.clearProcess=function()	{ this.process.length=0;}
	
	StringInputSettings.prototype.setRegExp=function(ex)	{ this.regexp=ex;		}
	StringInputSettings.prototype.clearRegExp=function()	{ this.regexp=null;		}
	
	StringInputSettings.prototype.setLanguage=function(lang)
	{
		if (!StringInputSettings.languages.hasOwnProperty(lang)) throw new Error('Unknown languages: '+lang);
		this.lang=StringInputSettings.languages[lang];
	}
	StringInputSettings.prototype.clearLanguage=function()	{ this.lang=null;		}

	
	StringInputSettings.LANG_EN=1;
	StringInputSettings.LANG_RU=2;
	StringInputSettings.LANG_JP=3;
	StringInputSettings.languages=
	{
		en:		StringInputSettings.LANG_EN,
		latin:	StringInputSettings.LANG_EN,
		eng:	StringInputSettings.LANG_EN,
		english:StringInputSettings.LANG_EN,
		
		ru:		StringInputSettings.LANG_RU,
		rus:	StringInputSettings.LANG_RU,
		russian:StringInputSettings.LANG_RU,
		cyr:	StringInputSettings.LANG_RU,
		cyrillic:StringInputSettings.LANG_RU,
		
		ja:		StringInputSettings.LANG_JP,
		jp:		StringInputSettings.LANG_JP,
		jap:	StringInputSettings.LANG_JP,
		japanese:StringInputSettings.LANG_JP
	}
	
	var _Game_Interpreter_clear=Game_Interpreter.prototype.clear;
    Game_Interpreter.prototype.clear = function()
	{
		_Game_Interpreter_clear.call(this);
		delete this._StringInput_settings;
	}
	
	var Scene_StringInput = function()
	{
		Scene_Name.apply(this, arguments);
	}
	EvilCat.extend(Scene_StringInput, Scene_Name);
	
	Scene_StringInput.prototype.prepare=function(minLength, maxLength, varname, settings)
	{
		this._maxLength	= maxLength;
		this._minLength	= minLength;
		this._varname	= varname;
		this._settings	= settings;
		this._actorId	= settings.actorId();
	}
	
	Scene_StringInput.prototype.createEditWindow=function()
	{
		this._editWindow = new Window_StringInput_Edit(this._minLength, this._maxLength, this._settings);
		this.addWindow(this._editWindow);
	}

	Scene_StringInput.prototype.createInputWindow=function()
	{
		this._inputWindow = new Window_StringInput(this._editWindow);
		this._inputWindow.setHandler('ok', this.onInputOk.bind(this));
		this.addWindow(this._inputWindow);
	}
	
	Scene_StringInput.prototype.onInputOk=function()
	{
		$gameVariables.setValue(this._varname, this._editWindow.name());
		this.popScene();
	};
	
	var Window_StringInput_Edit = function()
	{
		Window_NameEdit.apply(this, arguments);
	}
	EvilCat.extend(Window_StringInput_Edit, Window_NameEdit);

	Window_StringInput_Edit.prototype.initialize=function(minLength, maxLength, settings)
	{
		this.settings=settings;
		var actor=this.settings.actor();
		if (!actor) actor=$gameParty.leader(); // Window_NameEdit requires an actor, even if we don't intend to use one.
		this._minLength=minLength;
		
		Window_NameEdit.prototype.initialize.call(this, actor, maxLength);
		
		this._defaultName = this.settings.default;
		this.restoreDefault();
	}
	
	Window_StringInput_Edit.prototype.refresh=function()
	{
		this.refreshName();
		Window_NameEdit.prototype.refresh.call(this);
	}
	
	Window_StringInput_Edit.prototype.refreshName=function()
	{
		// processing has to be done on Ok, or you won't be able to input two words with a "trim" processor on.
	}
	
	Window_StringInput_Edit.prototype.setName=function(name)
	{
		if (name===this._name) return;
		this._name=name;
		this._index=name.length;
		this.refresh();
	}
	
	Window_StringInput_Edit.prototype.drawActorFace=function(actor, x, y, width, height)
	{
		if (!this.settings.face) return;
		if (this.settings.actorId()) Window_NameEdit.prototype.drawActorFace.call(this, actor, x, y, width, height);
		else this.drawFace(this.settings.face[0], this.settings.face[1], x, y, width, height);
	}
	
	var Window_StringInput = function()
	{
		Window_NameInput.apply(this, arguments);
	}
	EvilCat.extend(Window_StringInput, Window_NameInput);
	
	Window_StringInput.prototype.initialize=function(editWindow)
	{
		this.settings=editWindow.settings;
		Window_NameInput.prototype.initialize.call(this, editWindow);
	}
	
	Window_StringInput.prototype.table=function()
	{
		if (this.settings.lang===null) return Window_NameInput.prototype.table.call(this);
		
		if (this.settings.lang===StringInputSettings.LANG_JP) {
			return [Window_NameInput.JAPAN1,
					Window_NameInput.JAPAN2,
					Window_NameInput.JAPAN3];
		} else if (this.settings.lang===StringInputSettings.LANG_RU) {
			return [Window_NameInput.RUSSIA];
		} else if (this.settings.lang===StringInputSettings.LANG_EN) {
			return [Window_NameInput.LATIN1,
					Window_NameInput.LATIN2];
		}
		else throw new Error('Unknown language #'+this.settings.lang);
	};
	
	Window_StringInput.prototype.onNameOk=function()
	{
		if (this.isNameValid())
		{
			SoundManager.playOk();
			this.callOkHandler();
		}
		else SoundManager.playBuzzer();
	}
	
	Window_StringInput.prototype.isNameValid=function()
	{
		this.applyProcessors();
		var name=this._editWindow._name;
		if (name.length<this._editWindow._minLength) return false;
		if (name.length>this._editWindow._maxLength) return false;
		if (this.settings.verify && !this.settings.verify(name)) return false;
		if (this.settings.regexp && !this.settings.regexp.exec(name)) return false;
		return true;
	}
	
	Window_StringInput.prototype.applyProcessors=function()
	{
		var name=this._editWindow._name;
		this.settings.process.forEach((function(processor)
		{
			name=processor(name);
		}).bind(this));
		this._editWindow.setName(name);
	}
})();