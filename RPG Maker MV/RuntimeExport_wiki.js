//=============================================================================
// RuntimeExport_wiki.js
//=============================================================================

/*:
 * @plugindesc An add-on to RuntimeExport that adds formatted Project exporing.
 * @author EvilCat
 * @email soevilcat@mail.ru
 * @version 0.1.1
 
 * @param Skip plugins
 * @desc Plugins to skip while exporting
 * @default RuntimeExport, RuntimeExport_wiki
 
 * @help
 * No assemply required.
 * The JSON copied is formatted for use with WikiRPG engine.
 * Ids are replaced with titles (taken from either meta or name), so some functions,
 * such as adding a party member by variable content, only work if mathematical
 * operations are not involved. The export informs if some content has the same title
 * but differen data (same title and data points to a copied resource, especially events).
 * Which is WIP :(
 * Creative Commons 4.0 Attribution license
 */

 /*
 Event commands involving numerical content id:
 * Select Item
 * Control Variables
 * Conditional Branch
 * Common Event
 * All Party commands except Change Gold
 * All Actor commands
 * All Movement commands except Get on/off Vehicle
 * Show Animation
 * Show Balloon Icon
 * Show Picture
 * (Audio & Video commands already use titles)
 * Battle Processing
 * Shop Processing
 * Name Input Processing
 * Change Actor Images
 * Change Tileset
 * Enemy Transform
 * Show Battle Animation
 * Force Action
 */
 
"use strict";

if (!EvilCat) throw new Error('Requires EvilCat Utils plugin!');
if (!EvilCat.RuntimeExport) throw new Error('Requires RuntimeExport plugin!');
if (!EvilCat.WikiRPG) throw new Error('Requires WikiRPG plugin!');

{
	EvilCat.RuntimeExport.registerAddOn('RuntimeExport_wiki');
	
	let Window_ExportCommand=EvilCat.RuntimeExport.Window_ExportCommand;
	let Scene_Export=EvilCat.RuntimeExport.Scene_Export;
	
	let _makeCommandList=Window_ExportCommand.prototype.makeCommandList;
	Window_ExportCommand.prototype.makeCommandList=function()
	{
		_makeCommandList.apply(this, arguments);
		
		var nextCommand=this._list.pop();
		this.addCommand('Project', 'Copy Project');
		this._list.push(nextCommand);
	}
	
	let _maxCols=Window_ExportCommand.prototype.maxCols;
	Window_ExportCommand.prototype.maxCols=function()
	{
		return _maxCols.apply(this, arguments)+1;
	}
	
	let _prepareCommandWindow=Scene_Export.prototype.prepareCommandWindow;
	
	Scene_Export.prototype.prepareCommandWindow=function(win)
	{
		_prepareCommandWindow.apply(this, arguments);
		win.setHandler('Copy Project',  this.commandCopyProject.bind(this));
	}
	
	Scene_Export.prototype.commandCopyProject=function()
	{
		var json={};
		json['$dataSystem']=$dataSystem;
		var skipPlugins=EvilCat.RuntimeExport.paramArray('Skip plugins');
		json['$plugins']=$plugins.filter(function(data) { return skipPlugins.indexOf(data.name)===-1; });
		this.copy(json);
	}
}