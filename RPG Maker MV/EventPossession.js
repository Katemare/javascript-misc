//=============================================================================
// EventPossession.js
//=============================================================================

/*:
 * @plugindesc Allows Player to control an event.
 * @author EvilCat
 *
 * @param On Possession: Hide Player
 * @desc Turn this ON to automatically hide player on possession. Turn this OFF if you want to do it manually or not at all.
 * @default ON
 *
 * @param On Possession: Self Switch
 * @desc Set this to A, B, C, or D to automatically turn ON and OFF a self switch of the possessed event.
 * @default D
 *
 * @param Possession Switch
 * @desc Set this to a switch number to turn ON when Player is possessing an event, and OFF when the possession ends.
 * @default 
 *
 * @param Possession Event ID Var
 * @desc Set this to variable number to store current possessed Event ID in.
 * @default 
 *
 * @param Keep Player's Move Speed
 * @desc Turn this ON to use Player's speed while possessed instead of event's.
 * @default ON
 *
 * @param Possessable By Default
 * @desc By default, only characters are possessable. Turn this ON or OFF to make everything (un)possessable. See notes in plugin's help for details.
 * @default Characters
 *
 * @param Possessable By Common Event
 * @desc Set this to common event ID to be called to determine possession. See notes in plugin's help for details.
 * @default 
 *
 * @param Possessable Event ID Candidate Var
 * @desc Set this to a var number to write possession candidate event ID to before running the checker common event.
 * @default 
 *
 * @param By Common Event: Result
 * @desc Set this to a switch number to read common event result from. MUST be set if "Possessable By Common Event" is set!
 * @default 
 *
 * @help
 *
 * Plugin Command:
 *   EventPossession possess           # Possess this event (only for map events)
 *   EventPossession possess 3         # Possess map event ID 3
 *   EventPossession unpossess         # Unpossess the currently possessed event
 *   EventPossession possess_action    # Possess event by interaction (following the action key logic)
 *
 * Event Note:
 *   <possessable>         # Can be possessed (no common event call is made)
 *   <maybe_possessable>   # Can be possessed (common event call is made, if set up; otherwise, according to "Possessable by Default" parameter)
 *   <unpossessable>       # Can't be possessed (no common event call is made)
 */

/*:ru
 * @plugindesc Позволяет игроку вселяться в ивенты.
 * @author EvilCat
 *
 * @param On Possession: Hide Player
 * @desc Если ВКЛ, то игрок скрывается по вселении. Если ВЫКЛ, то игрок остаётся виден.
 * @default ВКЛ
 *
 * @param On Possession: Self Switch
 * @desc Установите A, B, C или D, чтобы включить (и потом выключить) соответствующий переключатель у одержимого события.
 * @default D
 *
 * @param Possession Switch
 * @desc Set Указанный переключатель будет включаться по вселении и выключаться по выселении.
 * @default 
 *
 * @param Possession Event ID Var
 * @desc Указанная переменная будет хранить номер текущего одержимого события.
 * @default 
 *
 * @param Keep Player's Move Speed
 * @desc Если ВКЛ, то будет использоваться скорость игрока вместо скорости одержимого события.
 * @default ON
 *
 * @param Possessable By Default
 * @desc Значение "Characters" значит, что вселиться можно только в ивенты-персонажи. Можно поставить ВКЛ или ВЫКЛ. См. инструкции.
 * @default Characters
 *
 * @param Possessable By Common Event
 * @desc Указанный номер глобального события будет запускаться, чтобы проверить, можно ли вселиться в данный ивент.
 * @default 
 *
 * @param Possessable Event ID Candidate Var
 * @desc В указанную переменную будет записываться номер ивента-кандидата на вселение.
 * @default 
 *
 * @param By Common Event: Result
 * @desc Из указанного переключателя будет считываться результат глобального события-проверки. Обязательно, если вы указали событие!
 * @default 
 *
 * @help
 *
 * Команды плагина:
 *   EventPossession possess           # Вселиться в данный ивент (только для ивентов на карте)
 *   EventPossession possess 3         # Вселиться в ивент с таким номером
 *   EventPossession unpossess         # Выселиться из ивента
 *   EventPossession possess_action    # Вселиться в ивент согласно логике взаимодействия (под персонажем или впереди него)
 *
 * В заметках ивентов:
 *   <possessable>         # В этот ивент можно вселиться (проверок не производится)
 *   <maybe_possessable>   # Возможно, в этот имвент можно вселиться (если настроено, вызовется глобальное событие; иначе по настройке "Possessable By Default")
 *   <unpossessable>       # В этот ивент нельзя вселиться (проверок не производится)
 */
 
"use strict";

// RPG Maker MV последней версии использует далеко не последний движок Хрома, поэтому в нём нет некоторых стандартных функций.
// следующие два блока - официальные "полифиллы" данных функций, добавляющие их в более старые версии.

// https://tc39.github.io/ecma262/#sec-array.prototype.includes
if (!Array.prototype.includes) {
  Object.defineProperty(Array.prototype, 'includes', {
    value: function(searchElement, fromIndex) {

      // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If len is 0, return false.
      if (len === 0) {
        return false;
      }

      // 4. Let n be ? ToInteger(fromIndex).
      //    (If fromIndex is undefined, this step produces the value 0.)
      var n = fromIndex | 0;

      // 5. If n ≥ 0, then
      //  a. Let k be n.
      // 6. Else n < 0,
      //  a. Let k be len + n.
      //  b. If k < 0, let k be 0.
      var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

      function sameValueZero(x, y) {
        return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
      }

      // 7. Repeat, while k < len
      while (k < len) {
        // a. Let elementK be the result of ? Get(O, ! ToString(k)).
        // b. If SameValueZero(searchElement, elementK) is true, return true.
        // c. Increase k by 1. 
        if (sameValueZero(o[k], searchElement)) {
          return true;
        }
        k++;
      }

      // 8. Return false
      return false;
    }
  });
}

// https://tc39.github.io/ecma262/#sec-array.prototype.find
if (!Array.prototype.find) {
  Object.defineProperty(Array.prototype, 'find', {
    value: function(predicate) {
     // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If IsCallable(predicate) is false, throw a TypeError exception.
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }

      // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
      var thisArg = arguments[1];

      // 5. Let k be 0.
      var k = 0;

      // 6. Repeat, while k < len
      while (k < len) {
        // a. Let Pk be ! ToString(k).
        // b. Let kValue be ? Get(O, Pk).
        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
        // d. If testResult is true, return kValue.
        var kValue = o[k];
        if (predicate.call(thisArg, kValue, k, o)) {
          return kValue;
        }
        // e. Increase k by 1.
        k++;
      }

      // 7. Return undefined.
      return undefined;
    }
  });
}

var EventPossession = {};

(function(CharacterClass)
{
	
/*
############################
### Обработка параметров ###
############################
*/	
	
	// строчки, считающиеся в параметрах за истину.
	EventPossession.trueString = ['on', '1', 'yes', 'y', 'да', 'д', 'выкл'];
	// строчки, считающиеся в параметрах за ложь.
	EventPossession.falseString = ['off', '0', 'no', 'n', 'нет', 'н', '-', '', 'вкл'];

	EventPossession.parseBoolParam = function(name, onDefault)
	{
		var param = this.parameters[name];
		if (param === undefined) return onDefault;
		if (this.isTrueString(param)) return true;
		if (this.isFalseString(param)) return false;
		throw new Error("EventPossession: Can't parse param - " + name);
	}
	
	EventPossession.isTrueString = function(value)
	{
		return this.trueString.includes(value.toLowerCase().trim());
	}
	
	EventPossession.isFalseString = function(value)
	{
		return this.falseString.includes(value.toLowerCase().trim());
	}
	
	EventPossession.parseEnumParam = function(name, set, onDefault)
	{
		var param = this.parameters[name];
		if (param === undefined) return onDefault;
		param = param.toLowerCase().trim();
		var found = set.find(function(element)
		{
			if (element === true) return EventPossession.isTrueString(param);
			else if (element === false) return EventPossession.isFalseString(param);
			else return element.toLowerCase() === param;
		});
		if (found === undefined) throw new Error("EventPossession: Can't parse param - " + name);
		return found;
	}
	
	EventPossession.parseIdParam = function (name, allowEmpty)
	{
		var param = this.getValidId(this.parameters[name]);
		if (!param)
		{
			if (!allowEmpty) throw new Error("EventPossession: Empty ID - " + name);
			else return param;
		}
		return param;
	}
	
	EventPossession.getValidId = function(value)
	{
		value = parseInt(value);
		if (value < 0) throw new Error("EventPossession: Negative ID");
		if (value == 0) return null;
		return value;
	}
	
	EventPossession.parseIdParamOptional = function (name)
	{
		return EventPossession.parseIdParam(name, true);
	}

/*
############################
###     Инициализация    ###
############################
*/	
	
	EventPossession.init = function()
	{
		this.parameters = PluginManager.parameters('EventPossession');
		
		this.autoHidePlayer = this.parseBoolParam("On Possession: Hide Player", true);
		this.autoSelfSwitch = this.parseEnumParam("On Possession: Self Switch", ['A', 'B', 'C', 'D', ''], '');
		if (this.autoSelfSwitch === '') this.autoSelfSwitch = false;
		
		this.keepPlayerSpeed = this.parseBoolParam("Keep Player's Move Speed", true);
		
		this.possessionSwitch = this.parseIdParamOptional("Possession Switch");
		this.possessionEventIdVar = this.parseIdParamOptional("Possession Event ID Var");
		
		this.possessableByCommonEvent = this.parseIdParamOptional("Possessable By Common Event");
		this.candidateEventIdVar = this.parseIdParamOptional("Possessable Event ID Candidate Var");
		this.possessableByDefault = this.parseEnumParam("Possessable By Default", [true, false, "Characters"], "Characters");
		if (this.possessableByCommonEvent)
		{
			this.commonEventResult = this.parseIdParam("By Common Event: Result");
		}
	}
	EventPossession.init();
	
/*
##############################################
### Работа с переменными и переключателями ###
##############################################
*/	
	
	EventPossession.setSelfSwitch = function(character, letter, value)
	{
		var key = [character._mapId, character._eventId, letter];
		$gameSelfSwitches.setValue(key, !!value);
	}
	
	EventPossession.setSelfSwitchOn = function(character, letter)
	{
		this.setSelfSwitch(character, letter, true);
	}
	
	EventPossession.setSelfSwitchOff = function(character, letter)
	{
		this.setSelfSwitch(character, letter, false);
	}
	
/*
##################################
### Работа с состоянием игрока ###
##################################
*/	
	
	EventPossession.preservePlayer = function(forced)
	{
		if (!this.playerState || forced)
		{
			this.playerState = new CharacterState($gamePlayer);
		}
		return this.playerState;
	}
	
	EventPossession.restorePlayer = function(keepState)
	{
		if (!this.playerState) return;
		var state = this.playerState;
		state.apply($gamePlayer);
		if (!keepState) delete this.playerState;
		return state;
	}
	
	EventPossession.hidePlayer = function()
	{
		$gamePlayer.setTransparent(true);
	}
	
	EventPossession.revealPlayer = function()
	{
		$gamePlayer.setTransparent(false);
	}
	
	function CharacterState(character)
	{
		if (character) this.record(character);
	}
	CharacterState.prototype =
	{
		basic: function()
		{
			this.moveSpeed = 4;
			this.walkAnime = true;
			this.stepAnime = false;
			this.directionFix = false;
			this.through = false;
			this.transparent = false;
			this.x = 0;
			this.y = 0;
			this.direction = 2;
		},
		
		record: function(character)
		{
			this.moveSpeed		= character.moveSpeed();
			this.walkAnime		= character.hasWalkAnime();
			this.stepAnime		= character.hasStepAnime();
			this.directionFix	= character.isDirectionFixed();
			this.through		= character.isThrough();
			this.transparent	= character.isTransparent();
			this.x				= character.x;
			this.y				= character.y;
			this.direction		= character.direction();
		},
		
		apply: function(character)
		{
			if (this.moveSpeed !== undefined) character.setMoveSpeed(this.moveSpeed);
			if (this.walkAnime !== undefined) character.setWalkAnime(this.walkAnime);
			if (this.stepAnme !== undefined) character.setStepAnime(this.stepAnme);
			if (this.directionFix !== undefined) character.setDirectionFix(this.directionFix);
			if (this.through !== undefined) character.setThrough(this.through);
			if (this.transparent !== undefined) character.setTransparent(this.transparent);
			if (this.direction !== undefined) character.setDirection(this.direction);
		},
		
		locate: function(character)
		{
			character.locate(this.x, this.y);
		}
	}
	
	CharacterState.copy = function(source, target)
	{
		var state = new CharacterState(source);
		state.apply(target);
	}
	
	CharacterState.copyAndLocate = function(source, target)
	{
		var state = new CharacterState(source);
		state.apply(target);
		state.locate(target);
	}
	
/*
######################################
### Работа с глобальными событиями ###
######################################
*/	
	
	EventPossession.callCommonEvent = function(eventId, byEvent)
	{
		var commonEvent = $dataCommonEvents[eventId];
		if (!commonEvent) throw new Error("EventPossession: Common event not found - " + eventId);
		var eventId = byEvent.eventId();

		var interpreter = new Game_Interpreter(1);
		interpreter.setup(commonEvent.list, eventId);
		return interpreter;
	}
	
	EventPossession.finishCommonEvent = function(eventId, byEvent)
	{
		var interpreter = this.callCommonEvent(eventId, byEvent)
		interpreter.update();
		if (interpreter.isRunning()) throw new Error("EventPossession: Non-instant common events not supported");
	}
	
/*
######################################
### Проверка возможности вселиться ###
######################################
*/	

	CharacterClass.prototype.isPossessable = function()
	{
		if (this._erased) return false;
		if (this._pageIndex < 0) return false;
		if (this.event().meta.possessable) return true;
		if (this.event().meta.unpossessable) return false;
		if (this.event().meta.maybe_possessable) return this.checkPossessable();
		if (EventPossession.possessableByDefault === 'Characters' && this.isObjectCharacter()) return false;
		if (EventPossession.possessableByCommonEvent) return this.checkPossessable();
		return EventPossession.possessableByDefault;
	}
	
	CharacterClass.prototype.checkPossessable = function()
	{
		if (EventPossession.candidateEventIdVar) $gameVariables.setValue(EventPossession.candidateEventIdVar, this._eventId);
		if (EventPossession.possessableByCommonEvent)
		{
			EventPossession.finishCommonEvent(EventPossession.possessableByCommonEvent, this);
			return $gameSwitches.value(EventPossession.commonEventResult);
		}
		return EventPossession.possessableByDefault!==false;
	}
	
/*
#######################################
### Собственно вселение и выселение ###
#######################################
*/	

	Game_Player.prototype.possess = function(character)
	{
		if (!character.isPossessable()) return;
		
		if (this.possessing) this.unpossess();
		this.possessing = character;
		
		EventPossession.preservePlayer();
		
		if (EventPossession.possessionSwitch) $gameSwitches.setValue(EventPossession.possessionSwitch, true);
		if (EventPossession.possessionEventIdVar) $gameVariables.setValue(EventPossession.possessionEventIdVar, character.eventId());
		
		if (EventPossession.autoSelfSwitch) EventPossession.setSelfSwitchOn(character, EventPossession.autoSelfSwitch);
		else character.refreshPossessed();
		$gamePlayer.copyPosition(character);
	}
	
	Game_Player.prototype.possessAction = function()
	{
		this.possessEventHere();
		this.possessEventThere();
	}
	
	Game_Player.prototype.possessEventHere = function()
	{
		this.possessMapEvent(this.x, this.y, false);
	};

	Game_Player.prototype.possessEventThere = function()
	{
		var direction = this.direction();
		var x1 = this.x;
		var y1 = this.y;
		var x2 = $gameMap.roundXWithDirection(x1, direction);
		var y2 = $gameMap.roundYWithDirection(y1, direction);
		this.possessMapEvent(x2, y2, true);
	};
	
	Game_Player.prototype.possessMapEvent = function(x, y, normal)
	{
		var events = $gameMap.eventsXy(x, y);
		for (var i = 0; i < events.length; i++)
		{
			event = events[i];
			if (event.isNormalPriority() === normal && event.isPossessable()) this.possess(event);
		}
	}
	
	Game_Player.prototype.unpossess = function()
	{
		if (!this.possessing) return;
		var character = this.possessing;
		delete this.possessing;
		if (EventPossession.possessionSwitch) $gameSwitches.setValue(EventPossession.possessionSwitch, false);
		if (EventPossession.possessionEventIdVar) $gameVariables.setValue(EventPossession.possessionEventIdVar, 0);
		if (EventPossession.autoSelfSwitch) EventPossession.setSelfSwitchOff(character, EventPossession.autoSelfSwitch);
		character.setupPage();
		EventPossession.restorePlayer();
	}
	
/*
#############################
### Состояние одержимости ###
#############################
*/	
	
	var oldPlayer_update = Game_Player.prototype.update;
	Game_Player.prototype.update = function()
	{
		oldPlayer_update.apply(this, arguments);
		if (this.possessing) this.updatePossessed();
	}
	
	Game_Player.prototype.updatePossessed = function()
	{
		this.possessing.updatePossession();
	}
	
	CharacterClass.prototype.updatePossession = Game_Vehicle.prototype.syncWithPlayer;
	
	var oldCharacterRefresh = CharacterClass.prototype.refresh;
	CharacterClass.prototype.refresh = function()
	{
		oldCharacterRefresh.call(this);
		if ($gamePlayer.possessing === this) this.refreshPossessed();
	}
	
	CharacterClass.prototype.refreshPossessed = function()
	{
		if (this._erased)
		{
			$gamePlayer.unpossess();
			return;
		}
		var playerState = EventPossession.preservePlayer();
		var eventState = new CharacterState(this);
		if (EventPossession.keepPlayerSpeed) eventState.moveSpeed = EventPossession.playerState.moveSpeed;
		eventState.apply($gamePlayer);
		if (EventPossession.autoHidePlayer) EventPossession.hidePlayer();
	}
	
/*
#########################################
### Одержимость в различных ситуациях ###
#########################################
*/	
	
	var oldPlayerPerformTransfer = Game_Player.prototype.performTransfer;
	Game_Player.prototype.performTransfer = function()
	{
		if (this.isTransferring() && this.possessing) this.unpossess();
		oldPlayerPerformTransfer.call(this);
	}
	
	var oldPlayerGetOnVehicle = Game_Player.prototype.getOnVehicle;
	Game_Player.prototype.getOnVehicle = function()
	{
		if (this.possessing) return false;
		oldPlayerGetOnVehicle.call(this);
	}
	
/*
#######################
### Команды плагина ###
#######################
*/	
	
	var oldInterpreterPluginCommand = Game_Interpreter.prototype.pluginCommand;
	Game_Interpreter.prototype.pluginCommand = function(command, args)
	{
		oldInterpreterPluginCommand.call(this, command, args);
		if (command === 'EventPossession')
		{
			var subcommand = args[0];
			if (subcommand === 'possess')
			{
				var targetId = EventPossession.getValidId(args[1]);
				var target;
				if ( (isNaN(targetId)) || (targetId == 0) ) target = this.character();
				else target = $gameMap.event(targetId);
				if (!target) throw new Error("EventPossession: No possession target!");
				$gamePlayer.possess(target);
			}
			else if (subcommand === 'unpossess')
			{
				$gamePlayer.unpossess();
			}
			else if (subcommand === 'possess_action')
			{
				$gamePlayer.possessAction();
			}
		}
	};
})(Game_Event);