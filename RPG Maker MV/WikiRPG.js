//=============================================================================
// WikiRPG.js
//=============================================================================

/*:
 * @plugindesc Common functions for creating and loading a MediaWiki-based RPG.
 * @author EvilCat
 * @email soevilcat@mail.ru
 * @version 0.1
 
 * @help
 * WIP
 * Creative Commons 4.0 Attribution license
 */
 
"use strict";

if (!EvilCat) throw new Error('Requires EvilCat Utils plugin!');

{
	let WikiRPG = EvilCat.WikiRPG = class WikiRPG extends EvilCat.Plugin
	{
		constructor()
		{
			super();
		}
		
		loadNow(filename)
		{
		}
		
		loadLazy(filename)
		{
		}
		
		// the RPG Maker engine doesn't always specify data type to loading functions; just as well, you don't want to have 15 additonal namespaces and their talk pages on your MediaWiki. Both WikiRPG engine and MediaWiki extension can deduce data type from JSON signature.
		recognizeData(json)
		{
			if (json.hasOwnProperty('characterName'))		return WikiRPG.DATA_ACTOR;
			if (json.hasOwnProperty('expParams'))			return WikiRPG.DATA_CLASS;
			if (json.hasOwnProperty('tpCost'))				return WikiRPG.DATA_SKILL;
			if (json.hasOwnProperty('consumable'))			return WikiRPG.DATA_ITEM;
			if (json.hasOwnProperty('wtypeId'))				return WikiRPG.DATA_WEAPON;
			if (json.hasOwnProperty('atypeId'))				return WikiRPG.DATA_ARMOR;
			if (json.hasOwnProperty('dropItems'))			return WikiRPG.DATA_ENEMY;
			if (json.hasOwnProperty('members'))				return WikiRPG.DATA_TROOP;
			if (json.hasOwnProperty('autoRemovalTiming'))	return WikiRPG.DATA_STATE;
			if (json.hasOwnProperty('animation1Hue'))		return WikiRPG.DATA_ANIMATION;
			if (json.hasOwnProperty('tilesetNames'))		return WikiRPG.DATA_TILESET;
			if (json.hasOwnProperty('trigger'))
			{
				if (json.hasOwnProperty('pages'))			return WikiRPG.DATA_EVENT;
				else										return WikiRPG.DATA_COMMON_EVENT;
			}
			if (json.hasOwnProperty('gameTitle'))			return WikiRPG.DATA_SYSTEM;
			if (json.hasOwnProperty('disableDashing'))		return WikiRPG.DATA_MAP;
		}
	}
	WikiRPG = EvilCat.WikiRPG = new WikiRPG();
	
	WikiRPG.DATA_ACTOR	=1;
	WikiRPG.DATA_CLASS	=2;
	WikiRPG.DATA_SKILL	=3;
	WikiRPG.DATA_ITEM	=4;
	WikiRPG.DATA_WEAPON	=5;
	WikiRPG.DATA_ARMOR	=6;
	WikiRPG.DATA_ENEMY	=7;
	WikiRPG.DATA_TROOP	=8;
	WikiRPG.DATA_STATE	=9;
	WikiRPG.DATA_ANIMATION	=10;
	WikiRPG.DATA_TILESET	=11;
	WikiRPG.DATA_COMMON_EVENT=12;
	WikiRPG.DATA_SYSTEM		=13;
	WikiRPG.DATA_MAP		=14;
	WikiRPG.DATA_EVENT		=15;
	
	let getWikiTitle=function(throwError)
	{
		var title;
		if (this.getMeta)
		{
			title=this.getMeta('WikiTitle', 'String', '');
			if (title) return title;
		}
		if (this.getDBData)
		{
			title=this.getDBData().name;
			if (title) return title;
		}
		if (throwError) throw new Error('No Wiki title!');
		return false;
	}
	
	Game_Item	.prototype.getWikiTitle	= getWikiTitle;
	Game_Actor	.prototype.getWikiTitle	= getWikiTitle;
	Game_Enemy	.prototype.getWikiTitle	= getWikiTitle;
	Game_Troop	.prototype.getWikiTitle	= getWikiTitle;
	Game_Map	.prototype.getWikiTitle	= getWikiTitle;
	Game_CommonEvent.prototype.getWikiTitle	= getWikiTitle;
	Game_Event	.prototype.getWikiTitle	= getWikiTitle;
	Game_Player	.prototype.getWikiTitle	= getWikiTitle;
	Game_Follower.prototype.getWikiTitle= getWikiTitle;
	
	/*
	let _Game_Actors_actor=Game_Actors.prototype.actor;
	Game_Actors.prototype.actor = function(actorId)
	{
        if (!this._data[actorId]) $dataActors[actorId]=EvilCat.WikiRPG.loadNow(this.actorFilename(actorId));
		_Game_Actors_actor.apply(this, arguments);
    }
	*/
}