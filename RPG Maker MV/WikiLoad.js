//=============================================================================
// WikiLoad.js
//=============================================================================

/*:
 * @plugindesc Allows to load content lazily from a MediaWiki server.
 * @author EvilCat
 * @email soevilcat@mail.ru
 * @version 0.2
 
 * @param Base URL
 * @desc MediaWiki instance with game data
 * @default http://maker.rpgverse.ru/w/
 
 * @help
 * This plugin allows for loading content lazily from a community-edited source,
 * as opposed to loading a complete pre-defined database. Also, database items
 * are identified by titles, not by numerical ids.
 * The plugin attempts to asyncroneously pre-load various content that may be used
 * in current scene, but if additional content is needed, it's loaded in blocking manner.
 * Wherever possible, the content is loaded from cache; or loaded via single request.
 * WIP
 * Creative Commons 4.0 Attribution license
 */

"use strict";

if (!EvilCat) throw new Error('Requires EvilCat Utils plugin!');
if (!EvilCat.WebLoad) throw new Error('Requires WebLoad plugin!');
if (!EvilCat.WikiRPG) throw new Error('Requires WikiRPG plugin!');

{
	let WikiLoad = EvilCat.WikiLoad = class WikiLoad extends EvilCat.WebLoad.constructor
	{
		init()
		{
			this.WikiSource=new EvilCat.WebLoad.classes.WikiSource(this.paramString('Base URL'));
		}
		
		sourceByName(command)
		{
			if (command==='WikiLoad') return this.WikiSource;
		}
	}
	WikiLoad = EvilCat.WikiLoad = new EvilCat.WikiLoad();
	
	EvilCat.WebLoad.classes.WikiSource=class extends EvilCat.WebLoad.classes.WebSource
	{
		makeUrl(filename)
		{
			return this.baseUrl()+'index.php?action=raw&maxage=300&title=JSON:'+this.wikiEncode(filename);
		}
		
		wikiEncode(title)
		{
			return encodeURIComponent(title.replace(' ', '_'));
		}
		
		/*
		isProjectLoading()		{ return EvilCat.toBool(this.projectLoading); }
		projectIsLoading()		{ this.projectLoading=true; }
		clearProjectLoading()	{ this.projectLoading=false; }
		
		loadProject(title)
		{
			if (this.isProjectLoading() || this.isInWikimode() ) return;
			this.projectIsLoading();
			
			this.loadFile(filename, (function(json)
				{
					this.clearProjectLoading();
					DataManager._databaseFiles.forEach(function(filename, varname)
					{
						if (!json[varname]) return;
						window[varname]=json[varname];
						DataManager.onLoad(json);
						this.wikiMode_on();
						SceneManager.goto(Scene_WikiBoot);
					});
				}).bind(this),
				function() { throw new Error('Can\'t load event!'); }
			);
		}
		
		isInWikimode() { return EvilCat.toBool(this.wikiMode); }
		wikiMode_on() { this.wikiMode=true; }
		*/
	}
	
	class Scene_WikiBoot extends Scene_Boot
	{
		// same as Scene_Boot, but without DataManager call.
		create()
		{
			Scene_WikiBoot.prototype.create.call(this);
			ConfigManager.load();
			this.loadSystemImages();
		}
	}
	
	WikiLoad.init();
}