//=============================================================================
// ReferByTitle.js
//=============================================================================

/*:
 * @plugindesc Allows to refer to actors, events and other objects by unique title.
 * @author EvilCat
 * @email soevilcat@mail.ru
 * @version 0.7
 
 * @param Event Palette Map ID
 * @desc ID of map to look for events absent on the current map.
 * @default 
 
 * @param Wikilinks
 * @desc Turns on and off wikilink functions (see help)
 * @default On
 
 * @help
 * Allows to retrieve various type of data by title, not numerical id.
 * This plugin is for use mostly by other plugins, although it allows
 * to copy events from other events on map or (if Palette Map ID is set)
 * palette map by stating [[Title]] in its note. For example:
 * If the note says [[Bat]], then the plugin will look for an event named
 * "Bat" in the current map and copy it, overwriting this event.
 * If Palette Map ID is set, it will also look for "Bat" in the palette map.
 * Palette option requires EvilCat.WebLoad plugin!
 *
 * The title should be unique among peers, otherwise you introduce uncertainty.
 * Titles are not mandatory, but only titled data can be retrieved.
 * By default, name is used as title; you can override this by adding
 * <Title: something> to notes (it will be trimmed from spaces).
 *
 * Plugin commands:
 *
 * ReferByTitle LoadEvent title
 *   loads data from this thus titled event into the current event
 *   for example: ReferByTitle LoadEvent Bat
 *
 * ReferByTitle LoadEvent title => id
 *   loads data from the thus titled event into event with stated id
 *   for example: ReferByTitle LoadEvent Bat => 20
 *
 * ReferByTitle SpawnEvent title at x,y
 *   creates a new event with an unused (ever in this map) id and data of the thus titled event.
 *   for example: ReferByTitle SpawnEvent Bat at 4,5
 *
 * WIP: currently only retrieves events.
 * Creative Commons 4.0 Attribution license
 */

"use strict";

if (!EvilCat) throw new Error('Requires EvilCat Utils plugin!');

(function()
{
	var ReferByTitle = EvilCat.ReferByTitle = function ReferByTitle()
	{
		EvilCat.Plugin.call(this);
		if (this.paramId('Event Palette Map Id', false)!==false && !EvilCat.WebLoad) throw new Error('Requires WebLoad plugin for Palette Map!');
	}
	EvilCat.extend(ReferByTitle, EvilCat.Plugin);
	
	ReferByTitle.prototype.makeCommandsList=function()
	{
		this.validCommands=['LoadEvent', 'SpawnEvent'];
	}
	
	ReferByTitle.prototype.LoadEvent=function(plugin_name, interpreter, title, targetId, third)
	{
		if (['>', '=>', 'into', 'to'].contains(targetId)) targetId=third;
		if (!targetId) targetId=interpreter.eventId();
		
		var target;
		if (typeof targetId !== 'string' || /^\d+$/.test(targetId)) target=$gameMap.event(targetId);
		else target=this.getEvent(targetId);
		
		if (!target) throw new NotFoundError("ReferByTitle LoadEvent: target event not found");
		target.initWikilink(title);
	}
	
	ReferByTitle.prototype.SpawnEvent=function(plugin_name, interpreter, title, x, y, more)
	{
		if (x==='at') { x=y; y=more; }
		if (x.indexOf(',')>=0) 
		{
			var temp=x.split(',');
			x=temp[0];
			if (temp[1]!=='') y=temp[1];
		}
		x=Number(x);
		y=Number(y);
		
		var newId=Game_Event.getNextFreeId();
		var event = Object.create(Game_Event.prototype);
		
		ReferByTitle.promiseWikiEventData(title).then(function(data)
		{
			data.x=x;
			data.y=y;
			event.setData(data);
			Game_Event.call(event, $gameMap.mapId(), newId);
			$gameMap.addEvent(event);
		},
		function(reason)
		{
			SceneManager.catchException(reason);
		});
	}
	
	/**
	* A plugin command to load named event.
	*/
	
	/**
	* Whether data in a cache map should expire.
	* @return bool
	* @private
	*/
	ReferByTitle.prototype._isPersistent=function(varname)
	{
		var volatileData=['_eventMap', '_mapsMap'];
		return volatileData.indexOf(varname)===-1;
	}
	
	/**
	* Retrieves a Map object to hold cache of specific data.
	* @return Map
	* @private
	*/
	ReferByTitle.prototype._getTitleCache=function(varname)
		{ return this[varname] || (this[varname] = new Map()); }
	
	/**
	* Clears a Map object with cache of specific data.
	* @private
	*/
	ReferByTitle.prototype._clearCache=function(varname)
		{ delete this[varname]; }
	
	/**
	* Retrieves a Map object to hold events on current map.
	* @return Map
	* @private
	*/
	ReferByTitle.prototype._events=function()
		{ return this._getTitleCache('_eventsMap');	}
	
	/**
	* Retrieves a Map object to hold events on the palette map.
	* @return Map
	* @private
	*/
	ReferByTitle.prototype._palette=function()
		{ return this._getTitleCache('_paletteMap');	}
	
	/*
	// Unimplemented yet
	ReferByTitle.prototype._actors=function()	{ return this._getTitleCache('_actorsMap');	}
	ReferByTitle.prototype._classes=function()	{ return this._getTitleCache('_classesMap');	}
	ReferByTitle.prototype._skills=function()	{ return this._getTitleCache('_skillsMap');	}
	ReferByTitle.prototype._items=function()	{ return this._getTitleCache('_itemsMap');	}
	ReferByTitle.prototype._weapons=function()	{ return this._getTitleCache('_weaponMap');	}
	ReferByTitle.prototype._armors=function()	{ return this._getTitleCache('_armorsMap');	}
	ReferByTitle.prototype._enemies=function()	{ return this._getTitleCache('_enemiesMap');	}
	ReferByTitle.prototype._troops=function()	{ return this._getTitleCache('_troopsMap');	}
	ReferByTitle.prototype._states=function()	{ return this._getTitleCache('_statesMap');	}
	ReferByTitle.prototype._anims=function()	{ return this._getTitleCache('_animsMap');	}
	ReferByTitle.prototype._tilesets=function()	{ return this._getTitleCache('_tilesetsMap');	}
	ReferByTitle.prototype._cEvents=function()	{ return this._getTitleCache('_cEventsMap');	}
	ReferByTitle.prototype._maps=function()		{ return this._getTitleCache('_mapsMap');		}
	*/
	
	
	/*
	// these functions are possible, but would work slow any way you look at it.
	
	ReferByTitle.prototype._all=function()		{ return this._getTitleCache('_allMap');		}
	ReferByTitle.prototype._globEvents=function() { return this._getTitleCache('_globEventsMap');	}
	
	*/
	
	/**
	* Tries to get data from cache or executes the find callback instead.
	* @return object
	* @private
	*/
	ReferByTitle.prototype._get_from_cache_or_find=function(title, cache, find_callback)
	{
		if (!title) return;
		var fromMap=cache.get(title);
		if (fromMap===false) return;
		else if (fromMap) return fromMap;
		if (cache.has(EvilCat.ReferByTitle.COMPLETED_KEY)) return;
		
		var find=find_callback();
		if (!cache.has(title))
		{
			if (find) cache.set(title, find);
			else cache.set(title, false);
		}
		return find;
	}
	
	/**
	* Finds an event object with desired title in the current map.
	* @return Game_Event
	* @throws NotFoundError if no such event is found
	*/
	ReferByTitle.prototype.getEvent=function(title, cache)
	{
		var cache = cache || this._events();
		return this._get_from_cache_or_find(title, cache, (function()
		{
			if (!$gameMap || !(SceneManager._scene instanceof Scene_Map)) return;
			var events=$gameMap.events();
			for (var x=0; x<events.length; x++)
			{
				var event=events[x];
				if (!event) continue;
				var eventTitle=event.getTitleId();
				if (!eventTitle) continue;
				if (!cache.has(eventTitle)) cache.set(eventTitle, event);
				if (eventTitle===title) return event;
			}
			cache.set(EvilCat.ReferByTitle.COMPLETED_KEY, true);
			throw new NotFoundError(title);
		}).bind(this));
	}
	
	/**
	* Promises to load the pallete map data. Shows "Loading" until resolved.
	* @return Promise that resolves to map JSON
	* @throws Error if palette map ID not set
	* @throws Error if EvilCat.WebLoad plugin not installed
	*/
	ReferByTitle.prototype.promisePaletteMap=function()
	{
		if (this.paletteMap instanceof Promise) return this.paletteMap;
		
		var paletteId=this.paramId('Event Palette Map ID', false);
		if (!paletteId) throw new Error('No palette map ID set');
		if (!EvilCat.WebLoad) throw new Error('Requires WebLoad plugin for Palette Map!');
		
		return this.paletteMap = EvilCat.WebLoad.StandardSource.syncLoadFile('Map'+paletteId.padZero(3));
	}
	
	/**
	* Promises to load event data with specific title from the palette map. Shows "Loading" until resolved.
	* @return Promise that resolves to event data.
	* @throws NotFoundError if no such event found.
	*/
	ReferByTitle.prototype.promisePaletteEventData=function(title)
	{
		var me=this, cache=this._palette();
		var result=this._get_from_cache_or_find(title, cache, function()
		{
			return me.promisePaletteMap().then(function(mapData)
			{
				for (var x=1; x<mapData.events.length; x++)
				{
					var event=mapData.events[x];
					var eventTitle=extractTitleId(event);
					if (!eventTitle) continue;
					
					if (!cache.has(eventTitle)) cache.set(eventTitle, event);
					if (eventTitle===title) return event;
				}
				cache.set(EvilCat.ReferByTitle.COMPLETED_KEY, true);
				throw new NotFoundError(title);
			}, function(reason) { SceneManager.catchException(new Error("Palette Map missing")); });
		});
		
		if (! (result instanceof Promise)) return Promise.resolve(result);
		return result;
	}
	
	/**
	* Promises to event data by wikilink; looks up in the current map first, then, if available, goes to lood in the palette map.
	* Shows "Loading" until resolved.
	* @return Promise that resolves to event data.
	* @throws NotFoundError if no such event found.
	*/
	ReferByTitle.prototype.promiseWikiEventData=function(wikilink)
	{
		var event;
		try { event=this.getEvent(wikilink); }
		catch (e)
		{
			if (e instanceof NotFoundError) event=null;
			else throw e;
		}
		if (event) return Promise.resolve(event.getDBData());
		
		if (this.paramId('Event Palette Map ID', false)===false) return Promise.reject(new NotFoundError(wikilink));
		return this.promisePaletteEventData(wikilink);
	}
	ReferByTitle = EvilCat.ReferByTitle = new ReferByTitle();
	
	ReferByTitle.COMPLETED_KEY=Symbol('completed');
	
	var NotFoundError=EvilCat.ReferByTitle.NotFoundError=function(title)
	{
		Error.apply(this, arguments);
		this.name='NotFoundError';
		this.message='Title: '+title;
	}
	EvilCat.extend(NotFoundError, Error);
	
	// ######################################
	// ### Adding brand new events to map ###
	// ######################################
	
	Game_Map.prototype.addEvent=function(event)
	{
		if (this._events[event.eventId()]) throw new Error("Event ID "+event.eventId()+" already exists!");
		this._events[event.eventId()]=event;
		
		if (this.runtimeEventsDelegate.length>0) this.runtimeEventsDelegate.forEach(function(delegate) { delegate(event); });
		
		this.requestRefresh();
	}
	
	var oldGameMap_initialize=Game_Map.prototype.initialize;
	Game_Map.prototype.initialize = function()
	{
		this.runtimeEventsDelegate=[];
		oldGameMap_initialize.call(this);
	}
	
	var oldSpritesetMap_initialize=Spriteset_Map.prototype.initialize;
	Spriteset_Map.prototype.initialize=function()
	{
		this.runtimeEventsDelegate=this.onRuntimeEventAdded.bind(this);
		$gameMap.runtimeEventsDelegate.push(this.runtimeEventsDelegate);
		oldSpritesetMap_initialize.apply(this, arguments);
	}
	
	Spriteset_Map.prototype.onRuntimeEventAdded=function(event)
	{
		var sprite=new Sprite_Character(event);
		this._characterSprites.push(sprite);
		this._tilemap.addChild(sprite);
	}
	
	var oldSceneMap_stop=Scene_Map.prototype.stop;
	Scene_Map.prototype.stop = function()
	{
		this._spriteset.onStop();
		oldSceneMap_stop.call(this);
	}
	
	Spriteset_Map.prototype.onStop=function()
	{
		var deleg=this.runtimeEventsDelegate;
		$gameMap.runtimeEventsDelegate=$gameMap.runtimeEventsDelegate.filter(function(delegate) { return delegate!==deleg; });
	}
	
	// if Wikilinks option is on, then at map's start fills each event that has a wikilink with data corresponding to that title.
	// displays "Loading" until completed.
	if (ReferByTitle.paramBool('Wikilinks', true))
	{
		// identifying wikilinks
		var _DataManager_extractMetadata=DataManager.extractMetadata;
		DataManager.extractMetadata = function(data)
		{
			_DataManager_extractMetadata.call(this, data);
			
			var re=/\[\[(.+?)\]\]/;
			var match = re.exec(data.note);
			if (match) data.meta.wikilink=match[1].trim();
		};
		
		// init wikilink for each event
		var _Game_Map_setupEvents=Game_Map.prototype.setupEvents;
		Game_Map.prototype.setupEvents = function()
		{
			_Game_Map_setupEvents.call(this);
			this.events().forEach(function(event)
			{
				var wikilink=event.getMeta('wikilink');
				if (wikilink) event.initWikilink(wikilink.trim());
			});
		}
		
		// request event data and apply when ready.
		Game_Event.prototype.initWikilink=function(wikilink)
		{
			wikilink=wikilink.trim();
			ReferByTitle.promiseWikiEventData(wikilink).then((function(data)
			{
				this.setData(data);
			}).bind(this),
			function(reason)
			{
				SceneManager.catchException(reason);
			});
		}
	}
	
	// forget event cache if scene is chaning.
	var Game_Map_setupEvents=Game_Map.prototype.setupEvents;
	Game_Map.prototype.setupEvents=function()
	{
		Game_Map_setupEvents.call(this);
		ReferByTitle._clearCache('_eventsMap');
	}
	
	/**
	* Extracts title from data.
	* @return string
	* @throws Error if throwError is true and no title is found
	* @private
	*/
	var extractTitleId=function(data, throwError)
	{
		if (!data.meta && data.note) DataManager.extractMetadata(data);
		if (data.meta)
		{
			var title=EvilCat.extractFromMeta(data.meta, 'Title', 'String', '').trim();
			if (title) return title;
		}
		if (data.name) return data.name;
		if (throwError) throw new Error('No title id!');
		return false;
	}
	
	/**
	* Extracts title from context ('this' object).
	* @return string
	* @throws Error if throwError evals to true and no title is found
	* @private
	*/
	var getTitleId=function(throwError)
	{
		if (this.hasOwnProperty('titleId')) return this.titleId;
		if (this.getDBData) return this.titleId=extractTitleId(this.getDBData(), throwError);
		if (throwError) throw new Error('No title id!');
		return false;
	}
	
	Game_Item	.prototype.getTitleId	= getTitleId;
	Game_Actor	.prototype.getTitleId	= getTitleId;
	Game_Enemy	.prototype.getTitleId	= getTitleId;
	Game_Troop	.prototype.getTitleId	= getTitleId;
	Game_Map	.prototype.getTitleId	= getTitleId;
	Game_CommonEvent.prototype.getTitleId= getTitleId;
	Game_Event	.prototype.getTitleId	= getTitleId;
	Game_Player	.prototype.getTitleId	= getTitleId;
	Game_Follower.prototype.getTitleId	= getTitleId;
	
})();