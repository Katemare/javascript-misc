//=============================================================================
// WebLoad.js
//=============================================================================

/*:
 * @plugindesc Loads data from web.
 * @author EvilCat
 * @email soevilcat@mail.ru
 * @version 0.3.1
 
 * @param Base URL
 * @desc Web folder with game data
 * @default http://pokeliga.com/test/
 
 * @param Max Request Time
 * @desc Number of seconds to wait for data
 * @default 5
 
 * @help
 * Use Plugin Command:
 * WebLoad loadEvent <filename>
 * WebLoad loadEventNow <filename>
 * ('.json' extension added automatically)
 * loadEvent is async while loadEventNow is sync (pauses the game until loaded).
 * Creative Commons 4.0 Attribution license
 */

"use strict";

if (!EvilCat) throw new Error('Requires EvilCat Utils plugin!');

(function()
{
	var WebLoad = EvilCat.WebLoad = function WebLoad()
	{
		EvilCat.Plugin.call(this);
	}
	EvilCat.extend(WebLoad, EvilCat.Plugin);

	WebLoad.prototype.init=function()
	{
		this.DefaultSource = this.StandardSource = new (EvilCat.WebLoad.classes.get('StandardSource'))();
		var baseUrl=this.paramString('Base URL', false);
		if (baseUrl!==false) this.WebSource=new (EvilCat.WebLoad.classes.get('WebSource'))(baseUrl);
	}
	
	WebLoad.prototype.makeCommandsList=function() { this.validCommands=['loadEvent', 'loadEventNow']; }
	
	WebLoad.prototype.loadEvent=function(plugin_name, interpreter, filename)
	{
		var target=interpreter.getContext();
		if (!target) throw new Error('WebLoad works for map events only!');
		var source=this.sourceByName(plugin_name);
		if (!source) throw new Error('Source unrecognized');
		return source.loadEvent(target, filename);
	}
	
	WebLoad.prototype.loadEventNow=function(plugin_name, interpreter, filename)
	{
		var target=interpreter.getContext();
		if (!target) throw new Error('WebLoad works for map events only!');
		var source=this.sourceByName(plugin_name);
		if (!source) throw new Error('Source unrecognized');
		return source.loadEventNow(target, filename);
	}
	
	WebLoad.prototype.sourceByName=function(command)
	{
		if (command==='WebLoad')
		{
			if (!this.WebSource) throw new Error('Web source not set!');
			return this.WebSource;
		}
	}
		
	WebLoad=EvilCat.WebLoad=new WebLoad();
	WebLoad.DEFAULT_AUTOFAIL_TIMESPAN=5; // 5 s
	WebLoad.classes=new Map();

	var DataSource = function()
	{
		this._loadingEvents=new Map();
		this._promises=new Map();
	}
	EvilCat.WebLoad.classes.set('DataSource', DataSource);
		
	DataSource.prototype.makeUrl=function(filename)
	{
		return this.baseUrl()+filename+'.json';
	}
	
	DataSource.prototype.baseUrl=function()
	{
		throw new Error('abstract method!');
	}
	
	DataSource.prototype.loadFile=function(filename, autofailTimeout, sync)
	{
		if (this._promises.has(filename)) return this._promises.get(filename); // If filename was requested with different timeout and synchronity within short amount of time, only the first set of params will count. But this situation is unlikely.
		
		if (autofailTimeout===undefined) autofailTimeout=WebLoad.paramFloat('Max Request Time', EvilCat.WebLoad.DEFAULT_AUTOFAIL_TIMESPAN);
		autofailTimeout*=1000;
		if (sync) SceneManager.recordPromise();
		var me=this;
		var promise=new Promise(function(resolve, reject)
		{
			var xhr = new XMLHttpRequest();
			var url = me.makeUrl(filename);
			xhr.timeout=autofailTimeout;
			xhr.open('GET', url);
			xhr.overrideMimeType('application/json');
			xhr.onload = function()
			{
				if (xhr.status < 400)
				{
					var json=JSON.parse(xhr.responseText);
					resolve(json);
				}
				else reject(new LoadError(filename, xhr.status));
			};
			xhr.onerror=function() { reject(new LoadError(filename)); }
			xhr.onloadend=function()
			{
				me._promises.delete(filename);
				if (sync) SceneManager.settlePromise();
			};
			xhr.send();
		});
		this._promises.set(filename, promise);
		return promise;
	}
	
	DataSource.prototype.syncLoadFile=function(filename, autofailTimeout)
	{
		return this.loadFile(filename, autofailTimeout);
	}
	
	DataSource.prototype._loadEvent=function(target, filename, sync)
	{
		if (this.isEventLoading(target)) return;
		this.eventIsLoading(target);
		
		var callback=(function(json)
		{
			this.clearEventLoading(target);
			target.setData(json);
		}).bind(this);
		var error_callback=function(reason) { throw new Error('Can\'t load event! '+reason); }
		
		this.loadFile(filename, undefined, sync).then(callback, error_callback);
	}
	
	DataSource.prototype.loadEvent=function(target, filename)		{ this._loadEvent(target, filename, false); }
	DataSource.prototype.loadEventNow=function(target, filename)	{ this._loadEvent(target, filename, true);	}
	
	DataSource.prototype.isEventLoading=function(event)		{ return this._loadingEvents.has(event.eventId());	}
	DataSource.prototype.eventIsLoading=function(event)		{ this._loadingEvents.set(event.eventId(), true);	}
	DataSource.prototype.clearEventLoading=function(event)	{ this._loadingEvents.delete(event.eventId());		}
	
	var LoadError=EvilCat.WebLoad.LoadError=function(filename, code)
	{
		Error.apply(this, arguments);
		this.name='LoadError';
		this.message=filename;
		if (code) this.message+=' ('+code+')';
	}
	EvilCat.extend(LoadError, Error);
	
	var StandardSource = function() { DataSource.call(this); }
	EvilCat.extend(StandardSource, DataSource);
	StandardSource.prototype.baseUrl=function() { return 'data/'; }
	
	EvilCat.WebLoad.classes.set('StandardSource', StandardSource);
	
	var WebSource=function(base)
	{
		DataSource.call(this);
		this.base=base;
	}
	EvilCat.extend(WebSource, DataSource);
	WebSource.prototype.baseUrl=function() { return this.base; }
	
	EvilCat.WebLoad.classes.set('WebSource', WebSource);
	
	WebLoad.promisesSymbol=Symbol('promises');
	var _SceneManager_initialize=SceneManager.initialize;
	SceneManager.initialize=function()
	{
		SceneManager[WebLoad.promisesSymbol]=0;
		_SceneManager_initialize.call(this);
	}
	
	var _SceneManager_update=SceneManager.update;
	SceneManager.update = function()
	{
		if (SceneManager[WebLoad.promisesSymbol]>0) Graphics.updateLoading();
		else _SceneManager_update.call(this);
	}
	
	SceneManager.recordPromise=function()
	{
		this[WebLoad.promisesSymbol]++;
		if (this[WebLoad.promisesSymbol]==1)
		{
			Graphics.startLoading();
		}
	}
	
	SceneManager.settlePromise=function()
	{
		this[WebLoad.promisesSymbol]--;
		if (this[WebLoad.promisesSymbol]===0)
		{
			Graphics.endLoading();
			this.requestUpdate();
		}
	}
	
	WebLoad.init();
})();